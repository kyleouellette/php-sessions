<?php

session_start();
if( !isset($_SESSION['login']) ){
   $_SESSION['login'] = array(
      'is_active' => false,
      'username' => null,
      'attempts' => 0
   );
}

$users = array(
   'kyle' => 'password',
   'dave' => 'zephyr'
);

function get_user($name, $password){
   global $users;
   if( array_key_exists($name, $users) ){
      if( $users[$name] === $password ){
         return true;
      }
   }
   return false;
}