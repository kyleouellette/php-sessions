<?php
require('config.php');
/**
 * We know if they're logged in, there is no need to show the login screen
 */
if( $_SESSION['login']['is_active'] ){
   header('location: about.php');
}

/**
 * since we see that the password is set through the POST method, 
 * we know that the user is trying to login. We can increment the counter 
 * and then check the creds
 */
if(isset($_POST['password'])){
   $username = $_POST['username'];
   $password = $_POST['password'];
   $_SESSION['login']['attempts']++;

   /**
    * Our custom function will return true/false based on the creds
    * Removing logic from the conditional and into a function cleans things up
    */
   if( get_user($username, $password) ){
      // save some session info
      $_SESSION['login']['is_active'] = true;
      // save more session info
      $_SESSION['login']['username'] = $username;
      // send them to the about page
      header('location:about.php');
   }
}
?>
<!DOCTYPE html>
<html>
<head>
   <title> Login // be valid bro </title>
</head>
<body>

   <?php if( $_SESSION['attempts'] > 0 ): ?>
      <h3>You've tried to login <?php echo $_SESSION['attempts'];?> times</h3>
   <?php endif;?>

   <form method="post">
      username:
      <input type="text" name="username" /><br>
      password:
      <input type="password" name="password" /><br>
      <button type="submit">Login</button>
   </form>

</body>
</html>