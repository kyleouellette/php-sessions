<?php
require('config.php');
$login = $_SESSION['login'];
if( ! $login['is_active'] ){
   header('location: index.php');
}
?>
<!DOCTYPE html>
<html>
<head>
   <title> Login // About </title>
</head>
<body>
<h2>This is the about page. You are currently logged in as <?php echo $login['username']; ?>.</h2>
<p>This session is going to stay alive until you quit out of your browser completelty or <a href="logout.php">logout</a>.</p>
</body>
</html>